#Autor: Juan Pablo Muñeton Cadavid
@stories
Feature: Utest registration
  As a user, I want to register on the Utest econ page in order to create a new user.

  @scenerio1
  Scenario Outline: Create a user account
    Given that Juan wants to create a user account on Utest

    When Enter information about him
      | strName   | strLastname   | strEmail   | strBirthmonth   | strBirthday   | strBirthyear   | strLangspoken   |
      | <strName> | <strLastname> | <strEmail> | <strBirthmonth> | <strBirthday> | <strBirthyear> | <strLangspoken> |

    And Enter your address
      | strCity   | strZipcode   | strCountry   |
      | <strCity> | <strZipcode> | <strCountry> |

    And Enter the information of your devices
      | strComputer   | strVersion   | strLanguage  | strMobiledevice   | strModel   | strOS  |
      | <strComputer> | <strVersion> | <strLanguage>| <strMobiledevice> | <strModel> | <strOS>|

    And Enter your password
      | strPassword   | strConfirmpassword    |
      | <strPassword> | <strConfirmpassword>  |

    Then create user account
      | strMessage  |
      | <strMessage> |

    Examples:
      | strName    | strLastname     | strEmail                       | strBirthmonth   | strBirthday   | strBirthyear   | strLangspoken   | strCity             | strZipcode   | strCountry   | strComputer   | strVersion   | strLanguage  | strMobiledevice   | strModel      | strOS  | strPassword       | strConfirmpassword  | strMessage                                                                |
      | Juan Pablo | Muneton Cadavid | juanp.mca@gmail.com            | March           | 26            | 1996           | spanish         | Medellin Colombia   | 051053       | Colombia     | Windows   | Windows 10 home   | Spanish     |  Asus             | zenfone 4 Z   | 6.0.1  | Jpmc1234567890*   | Jpmc1234567890*     | 'Welcome' |

