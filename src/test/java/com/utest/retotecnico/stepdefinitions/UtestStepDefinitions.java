package com.utest.retotecnico.stepdefinitions;
import com.utest.retotecnico.model.UtestData;
import com.utest.retotecnico.questions.Answer;
import com.utest.retotecnico.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

public class UtestStepDefinitions {

    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^that Juan wants to create a user account on Utest$")
    public void thatJuanWantsToCreateAUserAccountOnUtest() {
        OnStage.theActorCalled("Juan").wasAbleTo(OpenUp.thePage());
    }

    @When("^Enter information about him$")
    public void enterInformationAboutHim(List<UtestData> utestData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(InformationAbout.onThePage(utestData.get(0).getStrName(),utestData.get(0).getStrLastname(),utestData.get(0).getStrEmail(),utestData.get(0).getStrBirthmonth(),utestData.get(0).getStrBirthday(),utestData.get(0).getStrBirthyear(),utestData.get(0).getStrLangspoken()));
    }

    @When("^Enter your address$")
    public void enterYourAddress(List<UtestData> utestData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(Adress.onThePage(utestData.get(0).getStrCity(),utestData.get(0).getStrZipcode(),utestData.get(0).getStrCountry()));
    }

    @When("^Enter the information of your devices$")
    public void enterTheInformationOfYourDevices(List<UtestData> utestData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(Devices.onThePage(utestData.get(0).getStrComputer(),utestData.get(0).getStrVersion(),utestData.get(0).getStrLanguage(),utestData.get(0).getStrMobiledevice(),utestData.get(0).getStrModel(),utestData.get(0).getStrOS()));
    }

    @When("^Enter your password$")
    public void enterYourPassword(List<UtestData> utestData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(YourPassword.onThePage(utestData.get(0).getStrPassword(),utestData.get(0).getStrConfirmpassword()));
    }

    @Then("^create user account$")
    public void createUserAccount(List<UtestData> utestData)
            throws Exception {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(utestData.get(0).getStrMessage())));


    }


}
