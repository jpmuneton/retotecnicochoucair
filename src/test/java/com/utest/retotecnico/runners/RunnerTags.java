package com.utest.retotecnico.runners;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions (
        features = "src/test/resources/features/registroUtest.feature",
        tags = "@stories",
        glue = {"com.utest.retotecnico.stepdefinitions"},
        snippets = SnippetType.CAMELCASE)


public class RunnerTags {
}
