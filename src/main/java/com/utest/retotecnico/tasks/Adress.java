package com.utest.retotecnico.tasks;

import com.utest.retotecnico.userinterface.AdressPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Adress implements Task {

    private String strCity;
    private String strZipcode;
    private String strCountry;

    public Adress(String strCity, String strZipcode, String strCountry) {
        this.strCity = strCity;
        this.strZipcode = strZipcode;
        this.strCountry = strCountry;
    }

    public static Adress onThePage(String strCity, String strZipcode, String strCountry) {
        return Tasks.instrumented(Adress.class,strCity,strZipcode,strCountry);

    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(strCity).into(AdressPage.INPUT_CITY),
                Click.on(AdressPage.SELECT_CITY),
                Enter.theValue(strZipcode).into(AdressPage.INPUT_ZIP_CODE),
                Click.on(AdressPage.CLICK_COUNTRY),
                Enter.theValue(strCountry).into(AdressPage.INPUT_COUNTRY),
                Click.on(AdressPage.SELECT_COUNTRY),
                Click.on(AdressPage.NEXT_BUTTON)
        );
    }
}
