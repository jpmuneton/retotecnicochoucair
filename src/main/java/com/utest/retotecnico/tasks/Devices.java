package com.utest.retotecnico.tasks;

import com.utest.retotecnico.userinterface.DevicesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Devices implements Task{

    private String strComputer;
    private String strVersion;
    private String strLanguage;
    private String strMobiledevice;
    private String strModel;
    private String strOS;

    public Devices(String strComputer, String strVersion, String strLanguage, String strMobiledevice, String strModel, String strOS) {
        this.strComputer = strComputer;
        this.strVersion = strVersion;
        this.strLanguage = strLanguage;
        this.strMobiledevice = strMobiledevice;
        this.strModel = strModel;
        this.strOS = strOS;
    }

    public static Performable onThePage(String strComputer, String strVersion, String strLanguage, String strMobiledevice, String strModel, String strOS) {
        return Tasks.instrumented(Devices.class,strComputer,strVersion,strLanguage,strMobiledevice,strModel,strOS);
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(DevicesPage.CLICK_COMPUTER),
                Enter.theValue(strComputer).into(DevicesPage.INPUT_COMPUTER),
                Click.on(DevicesPage.SELECT_COMPUTER),

                Click.on(DevicesPage.CLICK_VERSION),
                Enter.theValue(strVersion).into(DevicesPage.INPUT_VERSION),
                Click.on(DevicesPage.SELECT_VERSION),

                Click.on(DevicesPage.CLICK_LANGUAGE),
                Enter.theValue(strLanguage).into(DevicesPage.INPUT_LANGUAGE),
                Click.on(DevicesPage.SELECT_LANGUAGE),

                Click.on(DevicesPage.CLICK_MOBILE_DEVICE),
                Enter.theValue(strMobiledevice).into(DevicesPage.INPUT_MOBILE_DEVICE),
                Click.on(DevicesPage.SELECT_MOBILE_DEVICE),

                Click.on(DevicesPage.CLICK_MODEL),
                Enter.theValue(strModel).into(DevicesPage.INPUT_MODEL),
                Click.on(DevicesPage.SELECT_MODEL),

                Click.on(DevicesPage.CLICK_OS),
                Enter.theValue(strOS).into(DevicesPage.INPUT_OS),
                Click.on(DevicesPage.SELECT_OS),

                Click.on(DevicesPage.NEXT_BUTTON)
        );
    }
}
