package com.utest.retotecnico.tasks;

import com.utest.retotecnico.userinterface.DevicesPage;
import com.utest.retotecnico.userinterface.YourPasswordPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class YourPassword implements Task{

    private String strPassword;
    private String strConfirmpassword;

    public YourPassword(String strPassword, String strConfirmpassword) {
        this.strPassword = strPassword;
        this.strConfirmpassword = strConfirmpassword;
    }

    public static Performable onThePage(String strPassword, String strConfirmpassword) {
        return Tasks.instrumented(YourPassword.class,strPassword,strConfirmpassword);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(strPassword).into(YourPasswordPage.INPUT_PASSWORD),
                Enter.theValue(strConfirmpassword).into(YourPasswordPage.INPUT_CONFIRM_PASSWORD),
                Click.on(YourPasswordPage.CHECK_NEWS),
                Click.on(YourPasswordPage.CHECK_PRIVACITY),
                Click.on(YourPasswordPage.CHECK_TERMS),
                Click.on(YourPasswordPage.NEXT_BUTTON)
        );
    }
}
