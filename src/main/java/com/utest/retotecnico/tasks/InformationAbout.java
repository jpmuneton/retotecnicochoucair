package com.utest.retotecnico.tasks;

import com.utest.retotecnico.userinterface.InformationAboutPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class InformationAbout implements Task {

    private String strName;
    private String strLastname;
    private String strEmail;
    private String strBirthmonth;
    private String strBirthday;
    private String strBirthyear;
    private String strLangspoken;

    public InformationAbout(String strName, String strLastname, String strEmail, String strBirthmonth, String strBirthday, String strBirthyear, String strLangspoken) {
        this.strName = strName;
        this.strLastname = strLastname;
        this.strEmail = strEmail;
        this.strBirthmonth = strBirthmonth;
        this.strBirthday = strBirthday;
        this.strBirthyear = strBirthyear;
        this.strLangspoken = strLangspoken;
    }

    public static InformationAbout onThePage(String strName, String strLastname, String strEmail, String strBirthmonth, String strBirthday, String strBirthyear, String strLangspoken) {
        return Tasks.instrumented(InformationAbout.class,strName,strLastname,strEmail,strBirthmonth,strBirthday,strBirthyear,strLangspoken);

    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(InformationAboutPage.JOIN_TODAY_BUTTON),
                Enter.theValue(strName).into(InformationAboutPage.INPUT_NAME),
                Enter.theValue(strLastname).into(InformationAboutPage.INPUT_LASTNAME),
                Enter.theValue(strEmail).into(InformationAboutPage.INPUT_EMAIL),
                SelectFromOptions.byVisibleText(strBirthmonth).from(InformationAboutPage.SELECT_BIRTHMONTH),
                SelectFromOptions.byVisibleText(strBirthday).from(InformationAboutPage.SELECT_BIRTHDAY),
                SelectFromOptions.byVisibleText(strBirthyear).from(InformationAboutPage.SELECT_BIRTHYEAR),
                Enter.theValue(strLangspoken).into(InformationAboutPage.INPUT_LANGSPOKEN),
                Click.on(InformationAboutPage.SELECT_LANGSPOKEN),
                Click.on(InformationAboutPage.NEXT_BUTTON)
        );
    }

}
