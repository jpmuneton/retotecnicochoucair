package com.utest.retotecnico.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class AdressPage {


    public static final Target INPUT_CITY = Target.the("where do we write the City")
            .located(By.id("city"));

    public static final Target SELECT_CITY = Target.the("where we select the City")
            .located(By.xpath("//div[@class='pac-item']"));

    public static final Target INPUT_ZIP_CODE = Target.the("where do we write the Zip Code")
            .located(By.id("zip"));

    public static final Target CLICK_COUNTRY = Target.the("where we active input Country")
            .located(By.xpath("//div[@class='ui-select']//div//span[@class='btn btn-default form-control ui-select-toggle']"));

    public static final Target INPUT_COUNTRY = Target.the("where do we write the Country")
            .located(By.xpath("//div[@class='ui-select']//input[@Type='search']"));

    public static final Target SELECT_COUNTRY = Target.the("where we select the Country")
            .located(By.xpath("//ul[(@class='ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu')]"));

    public static final Target NEXT_BUTTON = Target.the("button to next - Devices")
            .located(By.xpath("//a[contains(@class,'btn btn-blue')]"));




}
