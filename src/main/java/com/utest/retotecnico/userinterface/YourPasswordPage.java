package com.utest.retotecnico.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class YourPasswordPage {

    public static final Target INPUT_PASSWORD = Target.the("where do we write the password")
            .located(By.id("password"));

    public static final Target INPUT_CONFIRM_PASSWORD = Target.the("where do we write the confirm password")
            .located(By.id("confirmPassword"));

    public static final Target CHECK_NEWS = Target.the("where you subscribe to the news")
            .located(By.name("newsletterOptIn"));

    public static final Target CHECK_TERMS = Target.the("where you aceppt to the terms")
            .located(By.id("termOfUse"));

    public static final Target CHECK_PRIVACITY = Target.the("where you aceppt to the privacity")
            .located(By.id("privacySetting"));

    public static final Target NEXT_BUTTON = Target.the("button to next - complete Setup")
            .located(By.id("laddaBtn"));

    public static final Target WELCOME_MESSAGE = Target.the("Extract the welcome message")
            .located(By.xpath("//h1[contains(text(),'Welcome')]"));
}
