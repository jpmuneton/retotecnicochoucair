package com.utest.retotecnico.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class DevicesPage {

    public static final Target CLICK_COMPUTER = Target.the("where we active input Your Computer")
            .located(By.name("osId"));

    public static final Target INPUT_COMPUTER = Target.the("where do we write the Your Computer")
            .located(By.xpath("//div[@name='osId']//input[@Type='search']"));

    public static final Target SELECT_COMPUTER = Target.the("where we select the Your Computer")
                .located(By.xpath("//ul[(@class='ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu')]"));

    public static final Target CLICK_VERSION = Target.the("where we active input Version")
            .located(By.name("osVersionId"));

    public static final Target INPUT_VERSION = Target.the("where do we write the Version")
            .located(By.xpath("//div[@name='osVersionId']//input[@Type='search']"));

    public static final Target SELECT_VERSION = Target.the("where we select the Version")
            .located(By.xpath("//ul[(@class='ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu')]"));

    public static final Target CLICK_LANGUAGE = Target.the("where we active input Language")
            .located(By.name("osLanguageId"));

    public static final Target INPUT_LANGUAGE = Target.the("where do we write the Language")
            .located(By.xpath("//div[@name='osLanguageId']//input[@Type='search']"));

    public static final Target SELECT_LANGUAGE = Target.the("where we select the Language")
            .located(By.xpath("//ul[(@class='ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu')]"));

    public static final Target CLICK_MOBILE_DEVICE = Target.the("where we active input Your Mobile Device")
            .located(By.name("handsetMakerId"));

    public static final Target INPUT_MOBILE_DEVICE = Target.the("where do we write the Your Mobile Device")
            .located(By.xpath("//div[@name='handsetMakerId']//input[@Type='search']"));

    public static final Target SELECT_MOBILE_DEVICE = Target.the("where we select the Your Mobile Device")
            .located(By.xpath("//ul[(@class='ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu')]"));

    public static final Target CLICK_MODEL = Target.the("where we active input Your Model")
            .located(By.name("handsetModelId"));

    public static final Target INPUT_MODEL = Target.the("where do we write the Your Model")
            .located(By.xpath("//div[@name='handsetModelId']//input[@Type='search']"));

    public static final Target SELECT_MODEL = Target.the("where we select the Your Model")
            .located(By.xpath("//ul[(@class='ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu')]"));

    public static final Target CLICK_OS = Target.the("where we active input Your OS")
            .located(By.name("handsetOSId"));

    public static final Target INPUT_OS = Target.the("where do we write the Your OS")
            .located(By.xpath("//div[@name='handsetOSId']//input[@Type='search']"));

    public static final Target SELECT_OS = Target.the("where we select the Your OS")
            .located(By.xpath("//ul[(@class='ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu')]"));


    public static final Target NEXT_BUTTON = Target.the("button to next - Last Step")
            .located(By.xpath("//a[contains(@class,'btn btn-blue')]"));
}
