package com.utest.retotecnico.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class InformationAboutPage {

    public static final Target JOIN_TODAY_BUTTON = Target.the("button that shows us the form to add information about you")
            .located(By.xpath("//a[@class='unauthenticated-nav-bar__sign-up']"));

    public static final Target INPUT_NAME = Target.the("where do we write the Name")
            .located(By.id("firstName"));

    public static final Target INPUT_LASTNAME = Target.the("where do we write the Lastname")
            .located(By.id("lastName"));

    public static final Target INPUT_EMAIL = Target.the("where do we write the Email")
            .located(By.id("email"));

    public static final Target SELECT_BIRTHMONTH = Target.the("where we select the month of birth")
            .located(By.id("birthMonth"));

    public static final Target SELECT_BIRTHDAY = Target.the("where we select the day of birth")
            .located(By.id("birthDay"));

    public static final Target SELECT_BIRTHYEAR = Target.the("where we select the year of birth")
            .located(By.id("birthYear"));

    public static final Target INPUT_LANGSPOKEN = Target.the("where do we write the Language Spoken")
            .located(By.xpath("//input[@class='ui-select-search input-xs ng-pristine ng-untouched ng-valid ng-empty']"));

    public static final Target SELECT_LANGSPOKEN = Target.the("where we select the Language Spoken")
            .located(By.xpath("//span[@class='ui-select-choices-row-inner']"));

    public static final Target NEXT_BUTTON = Target.the("button to next - Adress")
            .located(By.xpath("//a[contains(@class,'btn btn-blue')]"));





}
