package com.utest.retotecnico.model;

public class UtestData {

    private String strName;
    private String strLastname;
    private String strEmail;
    private String strBirthmonth;
    private String strBirthday;
    private String strBirthyear;
    private String strLangspoken;
    private String strCity;
    private String strZipcode;
    private String strCountry;
    private String strComputer;
    private String strVersion;
    private String strLanguage;
    private String strMobiledevice;
    private String strModel;
    private String strOS;
    private String strPassword;
    private String strConfirmpassword;
    private String strMessage;

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public String getStrLastname() {
        return strLastname;
    }

    public void setStrLastname(String strLastname) {
        this.strLastname = strLastname;
    }

    public String getStrEmail() {
        return strEmail;
    }

    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    public String getStrBirthmonth() {
        return strBirthmonth;
    }

    public void setStrBirthmonth(String strBirthmonth) {
        this.strBirthmonth = strBirthmonth;
    }

    public String getStrBirthday() {
        return strBirthday;
    }

    public void setStrBirthday(String strBirthday) {
        this.strBirthday = strBirthday;
    }

    public String getStrBirthyear() {
        return strBirthyear;
    }

    public void setStrBirthyear(String strBirthyear) {
        this.strBirthyear = strBirthyear;
    }

    public String getStrLangspoken() {
        return strLangspoken;
    }

    public void setStrLangspoken(String strLangspoken) {
        this.strLangspoken = strLangspoken;
    }

    public String getStrCity() {
        return strCity;
    }

    public void setStrCity(String strCity) {
        this.strCity = strCity;
    }

    public String getStrZipcode() {
        return strZipcode;
    }

    public void setStrZipcode(String strZipcode) {
        this.strZipcode = strZipcode;
    }

    public String getStrCountry() {
        return strCountry;
    }

    public void setStrCountry(String strCountry) {
        this.strCountry = strCountry;
    }

    public String getStrComputer() {
        return strComputer;
    }

    public void setStrComputer(String strComputer) {
        this.strComputer = strComputer;
    }

    public String getStrVersion() {
        return strVersion;
    }

    public void setStrVersion(String strVersion) {
        this.strVersion = strVersion;
    }

    public String getStrLanguage() {
        return strLanguage;
    }

    public void setStrLanguage(String strLanguage) {
        this.strLanguage = strLanguage;
    }

    public String getStrMobiledevice() {
        return strMobiledevice;
    }

    public void setStrMobiledevice(String strMobiledevice) {
        this.strMobiledevice = strMobiledevice;
    }

    public String getStrModel() {
        return strModel;
    }

    public void setStrModel(String strModel) {
        this.strModel = strModel;
    }

    public String getStrOS() {
        return strOS;
    }

    public void setStrOS(String strOS) {
        this.strOS = strOS;
    }

    public String getStrPassword() {
        return strPassword;
    }

    public void setStrPassword(String strPassword) {
        this.strPassword = strPassword;
    }

    public String getStrConfirmpassword() {
        return strConfirmpassword;
    }

    public void setStrConfirmpassword(String strConfirmpassword) {
        this.strConfirmpassword = strConfirmpassword;
    }

    public String getStrMessage() {
        return strMessage;
    }

    public void setStrMessage(String strMessage) {
        this.strMessage = strMessage;
    }
}
